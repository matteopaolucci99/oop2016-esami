package a01b.e2;

import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

public class GUI {
	private Logic logic;
	private List<JButton> buttons = new LinkedList<>();
	private JFrame jf;

	public GUI(String fileName, int size) {
		this.jf = new JFrame();
		this.logic = new LogicImpl(fileName);
		JButton jbRes = new JButton("Reset");
		JPanel jp = new JPanel();
		jbRes.addActionListener(e -> {
			this.logic.reset();
			this.refresh();
		});
		ActionListener al = e -> {
			logic.hit();
			this.refresh();
		};
		
		for (int i = 0; i < size; i++) {
			final JButton jb = new JButton(String.valueOf(i));
			jb.setEnabled(false);
			jb.addActionListener(al);
			jp.add(jb);
			this.buttons.add(jb);
		}
		
		jp.add(jbRes);
		jf.getContentPane().add(jp);
		jf.pack();
		this.refresh();
	}

	private void refresh() {
		for (var btn : this.buttons) {
			if (this.logic.isEnabled(this.buttons.indexOf(btn))) {
				btn.setEnabled(true);
			} else {
				btn.setEnabled(false);
			}
		}
		this.jf.setVisible(true);
	}

}
