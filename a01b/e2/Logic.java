package a01b.e2;

public interface Logic {
	void hit();
	
	void reset();
	
	boolean isEnabled(int index);
}
