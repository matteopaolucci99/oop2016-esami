package a01b.e2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class LogicImpl implements Logic {
	private int enabledButton;
	private String fileName;
	private Iterator<Integer> sequence;
	
	
	public LogicImpl(String fileName) {
		this.fileName = fileName;
		this.sequence = this.getSequence();
		this.enabledButton =  this.sequence.hasNext() ? this.sequence.next() : -1;
	}

	private Iterator<Integer> getSequence() {
		List<Integer> tmp = new LinkedList<>();
		try (BufferedReader file = new BufferedReader(new FileReader(this.fileName))) {
			file.lines().forEach(s -> tmp.add(Integer.valueOf(s)));
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return tmp.iterator();	
	}

	@Override
	public void hit() {
		this.enabledButton = this.sequence.hasNext() ? this.sequence.next() : -1;
	}

	@Override
	public void reset() {
		this.sequence = this.getSequence();
		this.hit();
	}

	@Override
	public boolean isEnabled(int index) {
		return index == this.enabledButton;
	}

}
