package a01b.e1;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ListBuilderImpl<X> implements ListBuilder<X> {
	protected List<X> elements;
	private boolean built;
	
	public ListBuilderImpl() {
		this.elements = new LinkedList<>();
		this.built = false;
	}

	@Override
	public void addElement(X x) {
		if(this.built) {
			throw new IllegalStateException();
		}
		this.elements.add(x);
	}

	@Override
	public List<X> build() {
		if(this.built) {
			throw new IllegalStateException();
		}
		this.built = true;
		return Collections.unmodifiableList(this.elements);
	}

}
