package a01b.e1;

import java.util.Collection;
import java.util.List;

public class BuildersImpl implements Builders {

	@Override
	public <X> ListBuilder<X> makeBasicBuilder() {
		return new ListBuilderImpl<>();
	}

	@Override
	public <X> ListBuilder<X> makeBuilderWithSize(int size) {
		return new ListBuilderImpl<X>() {
			@Override
			public List<X> build() {
				if(this.elements.size() != size) {
					throw new IllegalStateException();
				}
				return super.build();
			}
		};
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElements(Collection<X> from) {
		return new ListBuilderImpl<X>() {
			@Override
			public void addElement(X x) {
				if(!from.contains(x)) {
					throw new IllegalArgumentException();
				}
				super.addElement(x);
			}
		};
 	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElementsAndWithSize(Collection<X> from, int size) {
		// TODO Auto-generated method stub
		return null;
	}

}
