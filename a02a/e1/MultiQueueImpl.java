package a02a.e1;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

public class MultiQueueImpl<T, Q> implements MultiQueue<T, Q> {
	
	private Map<Q, Queue<T>> queues;
	
	public MultiQueueImpl() {
		this.queues = new HashMap<>();
	}

	@Override
	public Set<Q> availableQueues() {
		return this.queues.keySet();
	}

	@Override
	public void openNewQueue(Q queue) {
		this.queues.put(queue, new LinkedList<>());
	}

	@Override
	public boolean isQueueEmpty(Q queue) {
		return this.queues.get(queue).isEmpty();
	}

	@Override
	public void enqueue(T elem, Q queue) {
		this.queues.get(queue).add(elem);
	}

	@Override
	public Optional<T> dequeue(Q queue) {
		return this.queues.get(queue).isEmpty() ? Optional.empty() : Optional.of(this.queues.get(queue).remove());
	}

	@Override
	public Map<Q, Optional<T>> dequeueOneFromAllQueues() {
		Map<Q, Optional<T>> tmp = new HashMap<>();
		this.queues.forEach((name, queue) -> {
			tmp.put(name, queue.isEmpty() ? Optional.empty() : Optional.of(queue.remove()));
		});
		return tmp;
	}

	@Override
	public Set<T> allEnqueuedElements() {
		return this.queues.entrySet().stream()
									 .flatMap(e -> e.getValue().stream())
									 .collect(Collectors.toSet());
	}

	@Override
	public List<T> dequeueAllFromQueue(Q queue) {
		List<T> tmp = new LinkedList<>();
		this.queues.get(queue).forEach(tmp::add);
		this.queues.get(queue).clear();
		return tmp;
	}

	@Override
	public void closeQueueAndReallocate(Q queue) {
		this.queues.entrySet().stream()
							  .filter(e -> !e.getKey().equals(queue))
							  .findAny()
							  .get()
							  .getValue()
							  .addAll(this.dequeueAllFromQueue(queue));
	}

}
