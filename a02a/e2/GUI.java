package a02a.e2;

import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI {
	private int size;
	private Logic logic;
	private JFrame jf;
	private List<JButton> buttons;

	public GUI(int size) {
		this.size = size;
		this.logic = new LogicImpl(size);
		this.jf = new JFrame();
		this.buttons = new LinkedList<>();
		JButton jbMov = new JButton("Move");
		JButton jbRes = new JButton("Reset");
		JPanel jp = new JPanel();

		ActionListener al = e -> {
			JButton tmp = (JButton) e.getSource();
			if (!tmp.getText().equals("X")) {
				this.logic.hit(this.buttons.indexOf(tmp));
				tmp.setEnabled(false);
			}
		};

		jbMov.addActionListener(e -> {
			this.logic.move();
			this.refresh();
		});

		jbRes.addActionListener(e -> {
			this.logic.reset();
			this.buttons.forEach(jb -> jb.setEnabled(true));
			this.refresh();
		});

		for (int i = 0; i < this.size; i++) {
			JButton jb = new JButton(" ");
			this.buttons.add(jb);
			jb.addActionListener(al);
			jp.add(jb);
		}

		jp.add(jbMov);
		jp.add(jbRes);
		this.jf.getContentPane().add(jp);
		this.jf.pack();
		this.refresh();
	}

	private void refresh() {
		for (var btn : buttons) {
			int index = this.buttons.indexOf(btn);
			btn.setText(this.logic.hasAnX(index) ? "X" : " ");
		}
		this.jf.setVisible(true);
	}
}
