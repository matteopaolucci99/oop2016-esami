package a02a.e2;

public interface Logic {
	void hit(int index);
	
	void move();
	
	void reset();
	
	boolean hasAnX(int index);
}
