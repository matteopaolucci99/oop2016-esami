package a02a.e2;

import java.util.LinkedList;
import java.util.List;

public class LogicImpl implements Logic {
	private List<Integer> activeButtons;
	private int xPosition;
	private int size;
	private boolean rightDirection;

	public LogicImpl(int size) {
		this.activeButtons = new LinkedList<>();
		for (int i = 0; i < size; i++) {
			this.activeButtons.add(i);
		}
		this.xPosition = 0;
		this.size = size;
		this.rightDirection = true;
	}

	@Override
	public void hit(int index) {
		if (this.activeButtons.contains(index)) {
			this.activeButtons.remove(this.activeButtons.indexOf(index));
		}

		if (index < this.xPosition) {
			this.activeButtons.removeIf(i -> i < index);
		} else {
			this.activeButtons.removeIf(i -> i > index);
		}
	}

	@Override
	public void move() {
		this.computeDirection();
		if (this.rightDirection && this.activeButtons.size() > 1) {
			this.xPosition++;
		} else if (!this.rightDirection && this.activeButtons.size() > 1) {
			this.xPosition--;
		}
	}

	@Override
	public void reset() {
		this.activeButtons.clear();
		for (int i = 0; i < this.size; i++) {
			this.activeButtons.add(i);
		}
		this.xPosition = 0;
	}

	@Override
	public boolean hasAnX(int index) {
		return index == xPosition;
	}

	private void computeDirection() {
		if (this.rightDirection && !this.activeButtons.contains(this.xPosition + 1)) {
			this.rightDirection = false;
		} else if (!this.rightDirection && !this.activeButtons.contains(this.xPosition - 1)) {
			this.rightDirection = true;
		}
	}
}
