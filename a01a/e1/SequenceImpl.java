package a01a.e1;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class SequenceImpl<X> implements Sequence<X> {
	private List<X> elements;
	
	public SequenceImpl(List<X> list) {
		this.elements = new LinkedList<X>(list);
	}

	@Override
	public Optional<X> getAtPosition(int position) {
		return (this.elements.size() - 1) >= position ? Optional.of(this.elements.get(position)) : Optional.empty();
	}

	@Override
	public int size() {
		return this.elements.size();
	}

	@Override
	public List<X> asList() {
		return this.elements;
	}

	@Override
	public void executeOnAllElements(Executor<X> executor) {
		this.elements.stream().forEach(executor::execute);
	}

}
