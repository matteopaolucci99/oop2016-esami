package a01a.e1;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SequenceBuilderImpl<X> implements SequenceBuilder<X> {
	private List<X> elements;
	private boolean built;

	public SequenceBuilderImpl() {
		this(Collections.emptyList());
	}
	
	public SequenceBuilderImpl(List<X> elems) {
		this.elements = new LinkedList<>(elems);
		this.built = false;
	}

	@Override
	public void addElement(X x) {
		this.elements.add(x);
	}

	@Override
	public void removeElement(int position) {
		this.elements.remove(position);
	}

	@Override
	public void reverse() {
		Collections.reverse(this.elements);
	}

	@Override
	public void clear() {
		this.elements.clear();
	}

	@Override
	public Optional<Sequence<X>> build() {
		if(this.built) {
			return Optional.empty();
		}
		this.built = true;
		return Optional.of(new SequenceImpl<>(this.elements));
	}

	@Override
	public Optional<Sequence<X>> buildWithFilter(Filter<X> filter) {
		if(this.built) {
			return Optional.empty();
		}
		this.built = true;
		List<X> list = this.elements.stream().filter(filter::check).collect(Collectors.toList());
		return list.isEmpty() ? Optional.empty() : Optional.of(new SequenceImpl<>(list));
	}

	@Override
	public <Y> SequenceBuilder<Y> mapToNewBuilder(Mapper<X, Y> mapper) {
		List<Y> list = this.elements.stream().map(x -> mapper.transform(x)).collect(Collectors.toList());
		return new SequenceBuilderImpl<Y>(list);
	}

}
