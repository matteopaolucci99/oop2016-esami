package a01a.e2;

import javax.swing.*;

public class GUI {
	private Logic logic;
    
    public GUI(String fileName){
    	this.logic = new LogicImpl(fileName);
        JFrame jf = new JFrame();
        JButton jbR = new JButton("RAND");
        JButton jbD = new JButton("DEC");
        JButton jbZ = new JButton("ZERO");
        JButton jbOK = new JButton("OK");
        jbOK.addActionListener(e -> logic.writeAllAndReset());
        jbR.addActionListener(e -> logic.random());
        jbD.addActionListener(e -> logic.dec());
        jbZ.addActionListener(e -> logic.zero());
        JPanel jp = new JPanel();
        jp.add(jbR);
        jp.add(jbD);
        jp.add(jbZ);
        jp.add(jbOK);
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);
    }

}
