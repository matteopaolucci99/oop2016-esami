package a01a.e2;

public interface Logic {
	void random();
	
	void dec();
	
	void zero();

	void writeAllAndReset();
}
