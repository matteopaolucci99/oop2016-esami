package a01a.e2;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class LogicImpl implements Logic {	
	private String fileName;
	private final static Random RANDOM;
	private int dec;
	private List<Integer> values;

	static {
		RANDOM = new Random();
	}
	
	public LogicImpl(String fileName) {
		this.dec = -1;
		this.fileName = fileName;
		this.values = new LinkedList<>();
	}

	@Override
	public void random() {
		this.values.add(RANDOM.nextInt(10) + 1);
	}

	@Override
	public void dec() {
		this.values.add(dec--);
	}

	@Override
	public void zero() {
		this.values.add(0);
	}

	private void writeOnFile() {
		try (DataOutputStream file = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(this.fileName)))) {
			for(int val : this.values) {
				file.writeInt(val);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void writeAllAndReset() {
		this.writeOnFile();
		this.values.forEach(System.out::println);
		this.values.clear();
	}
}
